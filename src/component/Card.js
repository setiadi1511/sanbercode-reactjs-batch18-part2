import React from 'react';
function Button(props) {
  console.log(props.data)
  return (
    <div>
      {props.data.map((item,i) => 
        <div className="card" key={i}>
            <div><img src={item.photo}/></div>
            <div className="info">
              <div><strong>{item.name}</strong></div>
              <div>{item.profession}</div>
              <div>{item.age + " years old"}</div>
            </div>
        </div>  
      )}
    </div>
  )
}
  

export default Button;