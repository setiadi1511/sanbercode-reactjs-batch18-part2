import React, { useEffect, useState } from "react";

function List(props) {


  const [aksi, setAksi] = useState(true);
  const [index, setIndex] = useState(0);
  const [nama, setNama] = useState('');
  const [harga, setHarga] = useState('');
  const [berat, setBerat] = useState('');
  const [dataSource, setDataSource] = useState([]);

  

  const handleForm = async (e)=>{
    e.preventDefault()
    let item = {
      nama:nama,
      harga:harga,
      berat:berat,
    }
    if(aksi){
      await setDataSource([...dataSource, item]);
    } else {
      dataSource[index] = item;
      await setDataSource(dataSource)
    }
    setNama('')
    setHarga('')
    setBerat('')
    setAksi(true)
  }

  const upd = async (item,index)=>{
    setNama(item.nama)
    setHarga(item.harga)
    setBerat(item.berat)
    setAksi(false)
    setIndex(index)
  }
  const del = async (item,index)=>{
    const rows = [...dataSource];
    rows.slice(index, 1);
    setDataSource(rows);
    // await dataSource.splice(index, 1);
    // await setDataSource(dataSource)
  }

  useEffect(() => {
    setDataSource(dataSource);
  },[dataSource]);
  return (
    <>
      <form onSubmit={handleForm}>
        <table>
          <tbody>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td><input type="text" name="nama" value={nama} onChange={e => setNama(e.target.value)}/></td>
            </tr>
            <tr>
              <td>Harga</td>
              <td>:</td>
              <td><input type="text" name="harga" value={harga} onChange={e => setHarga(e.target.value)}/></td>
            </tr>
            <tr>
              <td>Berat</td>
              <td>:</td>
              <td><input type="text" name="berat" value={berat} onChange={e => setBerat(e.target.value)}/></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><button>Save</button></td>
            </tr>
          </tbody>
        </table>
      </form>
      <table className="tabelBuah">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {
            dataSource.map((item, index)=>{
              return(                    
                <tr key={index}>
                  <td>{item.nama}</td>
                  <td>{item.harga}</td>
                  <td>{item.berat}</td>
                  <td><button onClick={upd.bind(this,item,index)}>update</button><button onClick={del.bind(this,item,index)}>delete</button></td>
                </tr>
              )
            })
          }
          
        </tbody>
      </table>
    </>
  )
}

export default List;