import React from "react";

class Form extends React.Component {
  state = {
    nama:'',
    harga:'',
    berat:'',
    aksi:'add',
    index:0,
    dataSource: []
  };
  handleForm = (e) => {
    e.preventDefault()
    const item = {
      nama: this.state.nama,
      harga: this.state.harga,
      berat: this.state.berat
    };
    
    if(this.state.aksi=='add'){
      var data = [...this.state.dataSource, item];
    } else {
      this.state.dataSource[this.state.index] = item;
      var data = this.state.dataSource;
    }

    this.setState({
      nama:'',
      harga:'',
      berat:'',
      aksi:'add',
      dataSource: data
    });
  };
  upd = (item,index) => {
    console.log(index);
    console.log(item);
    this.setState({
      nama: item.nama,
      harga: item.harga,
      berat: item.berat,
      aksi:'edit',
      index: index
    });
  };
  del = (item,index) => {
    var data = [...this.state.dataSource]; // make a separate copy of the array
    data.splice(index, 1);
    this.setState({
      dataSource: data
    });
  };
  render() {
    return (
      <div>
        <div className="">
          <form onSubmit={this.handleForm}>
            <table>
              <tbody>
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><input type="text" name="nama" value={this.state.nama} onChange={e => this.setState({nama:e.target.value})}/></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>:</td>
                  <td><input type="text" name="harga" value={this.state.harga} onChange={e => this.setState({harga:e.target.value})}/></td>
                </tr>
                <tr>
                  <td>Berat</td>
                  <td>:</td>
                  <td><input type="text" name="berat" value={this.state.berat} onChange={e => this.setState({berat:e.target.value})}/></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td><button>Save</button></td>
                </tr>
              </tbody>
            </table>
          </form>
          <table className="tabelBuah">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.dataSource.map((item, index)=>{
                  return(                    
                    <tr key={index}>
                      <td>{item.nama}</td>
                      <td>{item.harga}</td>
                      <td>{item.berat}</td>
                      <td><button onClick={this.upd.bind(this,item,index)}>update</button><button onClick={this.del.bind(this,item,index)}>delete</button></td>
                    </tr>
                  )
                })
              }
              
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
export default Form;