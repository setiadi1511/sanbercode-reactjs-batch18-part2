import React, {useContext} from "react"
import {BuahContext} from "./BuahContext"

const BuahList = () =>{
  const [buah, setBuah,id, setId,nama, setNama,harga, setHarga,berat, setBerat,fetchData,addData,updData,delData] = useContext(BuahContext)

  const upd = async (item, index) => {
    setId(item.id)
    setNama(item.name)
    setHarga(item.price)
    setBerat(item.weight)
  }
  const del = async (item, index) => {
    await delData(item);
    await fetchData();
  }

  return(
      <table className="tabelBuah">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {
            buah.map((item, index) => {
              return (
                <tr key={index}>
                  <td style={{textAlign:"left"}}>{item.name}</td>
                  <td style={{textAlign:"right"}}>{item.price}</td>
                  <td style={{textAlign:"right"}}>{item.weight / 1000 + ' kg'}</td>
                  <td><button onClick={upd.bind(this, item)}>update</button><button onClick={del.bind(this, item)}>delete</button></td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
  )

}

export default BuahList