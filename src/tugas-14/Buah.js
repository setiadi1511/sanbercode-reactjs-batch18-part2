import React from "react"
import {BuahProvider} from "./BuahContext"
import BuahList from "./BuahList"
import BuahForm from "./BuahForm.js"

const Buah = () =>{
  return(
    <BuahProvider>
      <BuahForm/>
      <BuahList/>
    </BuahProvider>
  )
}

export default Buah;