import React, { useEffect,useContext, useState } from "react"
import { BuahContext } from "./BuahContext"

const BuahForm = () => {
  const [buah, setBuah,id, setId,nama, setNama,harga, setHarga,berat, setBerat,fetchData,addData,updData] = useContext(BuahContext)

  const handleForm = async (e) => {
    e.preventDefault()
    let item = {
      name: nama,
      price: harga,
      weight: berat,
    }
    if (id) {
      await updData(item);
    } else {
      await addData(item);
    }
    setId(0)
    setNama('')
    setHarga('')
    setBerat('')
    await fetchData();
  }


  return (
    <>
      <form onSubmit={handleForm}>
        <table>
          <tbody>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td><input type="text" name="nama" value={nama} onChange={e => setNama(e.target.value)} /></td>
            </tr>
            <tr>
              <td>Harga</td>
              <td>:</td>
              <td><input type="number" name="harga" value={harga} onChange={e => setHarga(e.target.value)} /></td>
            </tr>
            <tr>
              <td>Berat</td>
              <td>:</td>
              <td><input type="number" name="berat" value={berat} onChange={e => setBerat(e.target.value)} /></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><button>Save</button></td>
            </tr>
          </tbody>
        </table>
      </form>
    </>
  )

}

export default BuahForm