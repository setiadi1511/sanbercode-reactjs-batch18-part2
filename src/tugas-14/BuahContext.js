import React, { useEffect, useState, createContext } from "react";
import axios from 'axios'

export const BuahContext = createContext();
export const BuahProvider = props => {

  const [buah, setBuah] = useState([]);
  const [id, setId] = useState("")
  const [nama, setNama] = useState("")
  const [harga, setHarga] = useState("")
  const [berat, setBerat] = useState("")

  const fetchData = async () => {
    const result = await axios(
      'http://backendexample.sanbercloud.com/api/fruits',
    );
    setBuah(result.data);
  };
  
  const addData = async (item) => {
    const result = await axios.post(
      'http://backendexample.sanbercloud.com/api/fruits',
      item
    );
  };
  const updData = async (item) => {
    const result = await axios.put(
      'http://backendexample.sanbercloud.com/api/fruits/' + id,
      item
    );
  };
  const delData = async (item) => {
    const result = await axios.delete(
      'http://backendexample.sanbercloud.com/api/fruits/' + item.id,
    );
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <BuahContext.Provider value={[buah, setBuah, id, setId,nama, setNama,harga, setHarga,berat, setBerat,fetchData,addData,updData,delData]}>
      {props.children}
    </BuahContext.Provider>
  );
};