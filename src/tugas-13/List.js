import React, { useEffect, useState } from "react";
import axios from 'axios'

function List(props) {

  const [aksi, setAksi] = useState(true);
  const [id, setId] = useState(0);
  const [nama, setNama] = useState('');
  const [harga, setHarga] = useState('');
  const [berat, setBerat] = useState('');
  const [dataSource, setDataSource] = useState([]);

  const handleForm = async (e) => {
    e.preventDefault()
    let item = {
      name: nama,
      price: harga,
      weight: berat,
    }
    if (id) {
      await updData(item);
    } else {
      await addData(item);
    }
    setId(0)
    setNama('')
    setHarga('')
    setBerat('')
    await fetchData();
  }

  const upd = async (item, index) => {
    setId(item.id)
    setNama(item.name)
    setHarga(item.price)
    setBerat(item.weight)
  }
  const del = async (item, index) => {
    await delData(item);
    await fetchData();
  }

  const fetchData = async () => {
    const result = await axios(
      'http://backendexample.sanbercloud.com/api/fruits',
    );
    setDataSource(result.data);
  };

  const addData = async (item) => {
    const result = await axios.post(
      'http://backendexample.sanbercloud.com/api/fruits',
      item
    );
  };
  
  const updData = async (item) => {
    const result = await axios.put(
      'http://backendexample.sanbercloud.com/api/fruits/' + id,
      item
    );
  };
  const delData = async (item) => {
    const result = await axios.delete(
      'http://backendexample.sanbercloud.com/api/fruits/' + item.id,
    );
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <form onSubmit={handleForm}>
        <table>
          <tbody>
            <tr>
              <td>Nama</td>
              <td>:</td>
              <td><input type="text" name="nama" value={nama} onChange={e => setNama(e.target.value)} /></td>
            </tr>
            <tr>
              <td>Harga</td>
              <td>:</td>
              <td><input type="number" name="harga" value={harga} onChange={e => setHarga(e.target.value)} /></td>
            </tr>
            <tr>
              <td>Berat</td>
              <td>:</td>
              <td><input type="number" name="berat" value={berat} onChange={e => setBerat(e.target.value)} /></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><button>Save</button></td>
            </tr>
          </tbody>
        </table>
      </form>
      <table className="tabelBuah">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {
            dataSource.map((item, index) => {
              return (
                <tr key={index}>
                  <td style={{textAlign:"left"}}>{item.name}</td>
                  <td style={{textAlign:"right"}}>{item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                  <td style={{textAlign:"right"}}>{item.weight / 1000 + ' kg'}</td>
                  <td><button onClick={upd.bind(this, item)}>update</button><button onClick={del.bind(this, item)}>delete</button></td>
                </tr>
              )
            })
          }

        </tbody>
      </table>
    </>
  )
}

export default List;