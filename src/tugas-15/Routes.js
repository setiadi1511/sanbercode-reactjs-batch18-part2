import React from "react";
import { Switch, Route } from "react-router";
import Home from "./../tugas-15/Home";
import Tugas9 from "./../tugas-15/Tugas-9";
import Tugas10 from "./../tugas-15/Tugas-10";
import Tugas11 from "./../tugas-15/Tugas-11";
import Tugas12 from "./../tugas-15/Tugas-12";
import Tugas13 from "./../tugas-15/Tugas-13";
import Tugas14 from "./../tugas-15/Tugas-14";
import {NavContext} from "./NavContext"
const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/tugas9">
        <Tugas9 />
      </Route>
      <Route path="/tugas10">
        <Tugas10 />
      </Route>
      <Route path="/tugas11">
        <Tugas11 />
      </Route>
      <Route path="/tugas12">
        <Tugas12 />
      </Route>
      <Route path="/tugas13">
        <Tugas13 />
      </Route>
      <Route path="/tugas14">
        <Tugas14 />
      </Route>
    </Switch>
  );
};

export default Routes;