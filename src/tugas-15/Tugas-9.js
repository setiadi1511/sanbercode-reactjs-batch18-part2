import React from 'react';
import Button from "./../tugas-9/Button";
import CheckBox from "./../tugas-9/CheckBox";
import Header from "./../tugas-9/Header";
import Input from "./../tugas-9/Input";
import Label from "./../tugas-9/Label";

var namaBuah = ['Semangka', 'Jeruk', 'Nanas', 'Salak', 'Anggur'];

function Tugas9(props) {
    return (
        <>
            <div className="tugas9">
                <div className="box">
                    <table>
                        <tbody>
                            <tr>
                                <td colSpan="2">
                                    <Header />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Label name="Nama Pelanggan" />
                                </td>
                                <td>
                                    <Input />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Label name="Daftar Item" />
                                </td>
                                <td>
                                    {namaBuah.map((name, index) => <CheckBox key={index} name={name} />)}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2"><Button /></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                </div>
            </div>

        </>
    )
}


export default Tugas9;