import React from "react";
import {NavLink as Link} from "react-router-dom";
import './../App.css';
import {NavContext} from "./NavContext"
const Nav = () => {

  return (
    
    <div className="nav">
      <ul>
        <li>
          <Link to="/" exact={true}>Home</Link>
        </li>
        <li>
          <Link to="/tugas9">Pengenalan Reactjs</Link>
        </li>
        <li>
          <Link to="/tugas10">Component & Props</Link>
        </li>
        <li>
          <Link to="/tugas11">State & Components Lifecycle</Link>
        </li>
        <li>
          <Link to="/tugas12">Lists & Forms</Link>
        </li>
        <li>
          <Link to="/tugas13">Hooks & axios</Link>
        </li>
        <li>
          <Link to="/tugas14">Context</Link>
        </li>
      </ul>
    </div>
  )
}

export default Nav