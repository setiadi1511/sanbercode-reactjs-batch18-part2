import React from 'react';
import Button from "../tugas-9/Button";
import CheckBox from "../tugas-9/CheckBox";
import Header from "../tugas-9/Header";
import Input from "../tugas-9/Input";
import Label from "../tugas-9/Label";
import Table from '../tugas-10/Table';

let namaBuah = ['Semangka', 'Jeruk', 'Nanas', 'Salak', 'Anggur'];
let dataHargaBuah = [
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 }
];

dataHargaBuah.filter((item) => {
    item.berat = item.berat / 1000 + ' kg';
    return item;
})


function Tugas10(props) {
    return (
        <>
            <div className="tugas10">
                <div className="box">
                    <table>
                        <tbody>
                            <tr>
                                <td colSpan="2">
                                    <Header />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Label name="Nama Pelanggan" />
                                </td>
                                <td>
                                    <Input />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Label name="Daftar Item" />
                                </td>
                                <td>
                                    {namaBuah.map((name, index) => <CheckBox key={index} name={name} />)}
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2"><Button /></td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                </div>
                <div className="tabelHarga">
                    <Table label="Tabel Harga Buah" data={dataHargaBuah} />
                </div>

            </div>

        </>
    )
}


export default Tugas10;