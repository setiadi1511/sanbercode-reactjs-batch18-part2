import React, { useState, createContext } from "react";

export const NavContext = createContext();
export const NavProvider = props => {

  const [select, setSelect] = useState("");
  
  return (
    <NavContext.Provider value={[select, setSelect]}>
      {props.children}
    </NavContext.Provider>
  );
};