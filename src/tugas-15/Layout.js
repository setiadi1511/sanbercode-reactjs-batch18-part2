import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import './../App.css';
import Routes from './Routes';
import Nav from './Nav';
import {NavContext} from "./NavContext"


function App() {
  return (
    <>
      <Router>
        <Nav></Nav>
        <br/>
        <Routes/>
      </Router>   
    </>
    );
  }

export default App;
