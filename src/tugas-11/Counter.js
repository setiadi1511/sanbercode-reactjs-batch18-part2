import React, { useEffect, useState } from "react";

function Counter(props) {

  const counter = 100;

  const [count, setCount] = useState(counter);

  useEffect(() => {
    setTimeout(() => {
      setCount(count - 1 == 0 ? counter : count - 1);
    }, 1000);
  });

  return (<p>hitung mundur:  {count}</p>)
}

export default Counter;