import React, { useEffect, useState } from "react";

function Timer(props) {
  const calculateTime = () => {
    let date = new Date();
    return `sekarang jam : ${date.getHours()} : ${date.getMinutes()} : ${date.getSeconds()} ${date.getHours() >= 12 ? 'PM' : 'AM'}`;
  }

  const [timeLeft, setTimeLeft] = useState(calculateTime());

  useEffect(() => {
    setTimeout(() => {
      setTimeLeft(calculateTime());
    }, 1000);
  });

  return (<p>{timeLeft}</p>)
}

export default Timer;